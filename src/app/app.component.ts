import { Component, OnInit } from '@angular/core';
import '../assets/jscript/theme-scripts.min.js';
import { Router } from '@angular/router';
import { PostsService} from './services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tags: any;

  constructor(public router: Router, private postService: PostsService) {}

  public href: string= "";

  ngOnInit() {
    this.href = this.router.url;
    this.loadTags();
  }

  loadTags() {
    this.postService.getTags().subscribe((data) => {
        this.tags = data;
    });
  }


}
