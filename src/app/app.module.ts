import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { PostsService } from './services/posts.service';

import { OwlModule } from 'ngx-owl-carousel';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/partials/header/header.component';
import { HomeComponent } from './components/pages/home/home.component';
import { PostslideComponent } from './components/partials/postslide/postslide.component';
import { CategoryComponent } from './components/pages/category/category.component';
import { PostComponent } from './components/pages/post/post.component';
import { TagComponent } from './components/pages/tag/tag.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    PostslideComponent,
    CategoryComponent,
    PostComponent,
    TagComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    OwlModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: ':post_slug',
        component: PostComponent,
      },
      {
        path: 'kategoria',
        children: [
          {
            path: ':category_slug',
            component: CategoryComponent,
          }
        ]
      },
      {
        path: 'cimke',
        children: [
          {
            path: ':tag_slug',
            component: TagComponent,
          }
        ]
      },
    ])
  ],
  providers: [
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
