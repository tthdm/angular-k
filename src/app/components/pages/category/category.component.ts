import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PostsService} from '../../../services/posts.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  slug: string;
  categoryWithPosts: any;

  constructor(
    private route: ActivatedRoute,
    public postsService: PostsService,
    private router: Router
  ) {
    this.route.params.subscribe(params => {
      this.slug = params['category_slug'];
      this.loadPostsByCategory(this.slug);
      window.scroll(0, 0);
    });
  }

  ngOnInit() {
  }

  loadPostsByCategory(slug: string) {
    this.postsService.getPostsByCategory(slug).subscribe((data) => {
        this.categoryWithPosts = data;
    });
  }

}
