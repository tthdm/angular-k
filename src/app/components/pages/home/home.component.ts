import { Component, OnInit, ViewChild } from '@angular/core';
import { PostsService} from '../../../services/posts.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allCategoryPosts: object;
  categoriesWithPosts: any;
  posts1: any;
  posts2: any;
  category1: string = 'Terméktesztek';
  category2: string = 'Hírek';

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.loadCategoriesWithPost();
    this.loadLatestPosts();
    this.loadReviewPosts();
    this.loadNewsPosts();
  }

  loadCategoriesWithPost() {
    this.postsService.getCategoriesWithPosts().subscribe((data) => {
      this.categoriesWithPosts = data;
    });
  }

  loadLatestPosts() {
    this.postsService.getLatestPosts().subscribe((data) => {
      this.allCategoryPosts = data;
    });
  }

  loadReviewPosts() {
    this.postsService.getPostsByCategory('termektesztek').subscribe((data) => {
      this.posts1 = data;
    });
  }

  loadNewsPosts() {
    this.postsService.getPostsByCategory('hirek').subscribe((data) => {
      this.posts2 = data;
    });
  }

  loadOtTab() {
    $(".ot-title-selector > a, .ot-title-selector-drop > span a").on("click", function() {
      let e = $(this),
        t = e.data("ot-tab");
      return e.parents(".ot-title-selector").find("a.active").removeClass("active"), e.addClass("active").parents(".ot-title-block").next().find("[data-ot-tab='" + t + "']").addClass("active").siblings(".active").removeClass("active"), !1
    });
    $(".ot-title-selector-drop > a").on("click", function() {
      return $(this).parent().toggleClass("active"), !1
    });
    $(".ot-title-selector-drop").on("mouseleave", function() {
      return $(this).removeClass("active"), !1
    });
  }

}
