import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PostsService} from '../../../services/posts.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  slug: string;
  post: any;

  constructor(
    private route: ActivatedRoute,
    public postsService: PostsService,
    private router: Router
  ) {
    this.route.params.subscribe(params => {
      this.slug = params['post_slug'];
      this.loadPost(this.slug);
      window.scroll(0, 0);
    });
  }

  ngOnInit() {
  }

  loadPost(slug: string) {
      this.postsService.getPostBySlug(slug).subscribe((data) => {
          this.post = data;
      });
  }

}
