import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PostsService} from '../../../services/posts.service';

@Component({
    selector: 'app-tag',
    templateUrl: './tag.component.html',
    styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

    slug: string;
    tagWithPosts: any;

    constructor(
        private route: ActivatedRoute,
        public postsService: PostsService,
        private router: Router
    ) {
        this.route.params.subscribe(params => {
            this.slug = params['tag_slug'];
            this.loadPostsByTag(this.slug);
            window.scroll(0, 0);
        });
    }

    ngOnInit() {
    }

    loadPostsByTag(slug: string) {
        this.postsService.getPostsByTag(slug).subscribe((data) => {
            this.tagWithPosts = data;
        });
    }

}
