import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../../services/posts.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  menus: any;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.loadMenu();
  }

  loadMenu() {
    this.postsService.getMenu().subscribe((data: any) => {
        this.menus = data;
    });
  }

}
