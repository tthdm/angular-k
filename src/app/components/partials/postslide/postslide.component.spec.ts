import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostslideComponent } from './postslide.component';

describe('PostslideComponent', () => {
  let component: PostslideComponent;
  let fixture: ComponentFixture<PostslideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostslideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostslideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
