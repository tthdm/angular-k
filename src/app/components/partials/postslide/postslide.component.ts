import { Component, OnInit } from '@angular/core';
import { PostsService} from '../../../services/posts.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-postslide',
  templateUrl: './postslide.component.html',
  styleUrls: ['./postslide.component.css']
})
export class PostslideComponent implements OnInit {
  latestPosts: any;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.loadLatestPosts();
  }

  loadLatestPosts() {
    this.postsService.getLatestPosts().subscribe((data) => {
      this.latestPosts = data;
    });
  }

}
