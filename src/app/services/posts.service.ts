import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AppConfig } from '../config/app';


@Injectable()
export class PostsService {

  constructor(private http: HttpClient) { }

  getMenu() {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/menu');
  }

  getLatestPosts() {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/posts/latest');
  }

  getCategoriesWithPosts() {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/categories/withPosts');
  }

  getPostsByCategory(slug: string) {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/posts/byCategory/' + slug);
  }

  getPostsByTag(slug: string) {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/posts/byTag/' + slug);
  }

  getPostBySlug(slug: string) {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/post/' + slug);
  }

  getTags() {
    return this.http.get(AppConfig.apiUrl + 'api/posts/get/tags');
  }



}
